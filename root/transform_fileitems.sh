#!/bin/sh
####### USAGE #########
#./transform_fileitems.sh file.xml  ##### transforms file items xml document to csv structure

file=$1

if [ -z $file ]; then
    echo "Usage:"
    echo "./transform_fileitems.sh <path-to-xml-file>"
    exit
fi

xsltproc fileitems.xsl "$file"

