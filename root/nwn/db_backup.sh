#!/bin/sh
cd /home/virgil/nwn/
password="nwn.1l4ndr142"
user="root"
db_name="nwn"

# Other options
backup_path="../backup"
date=$(date +"%Y%m%d_%H%M%S")

# Dump database into SQL file
file=$db_name"_db_"$date

mysqldump -u $user -p$password $db_name > $backup_path/$file".sql"
tar -zcf $backup_path/$file".tgz" $backup_path/$file".sql"
rm -f $backup_path/$file".sql"

# Delete files older than 30 days
#find $backup_path/* -mtime +30 -exec rm {} \;
