#!/bin/sh
cd /home/virgil/nwn
./server_kill.sh
./server_loaddb.sh

cd ..
./check_for_updates.sh 0

cd /home/virgil/nwn
./server_start.sh
