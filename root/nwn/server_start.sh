#!/bin/sh
cd /home/virgil/nwn/
../decompress_file il2_dialog.zip
../decompress_file il2_override.zip

echo "Starting public server..."
export LD_PRELOAD=./nwnx2.so

./nwserver \
	-publicserver 1 \
	-port 5121 \
	-servername "Ilandria 2" \
	-dmpassword "il2-dm" \
	-oneparty 0 \
	-pvp 2 \
	-difficulty 4 \
	-elc 0 \
	-ilr 0 \
	-reloadwhenempty 0 \
	-module "vg_mod_master" \
	-maxclients 64 \
	-servervault 1 \
	-maxlevel 40 \
	-gametype 0 \
	-autosaveinterval 0 \
	"$@" \
>/dev/null &
