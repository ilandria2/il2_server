#--create database if not exists nwn character set utf8 collate utf8_unicode_ci;
#--use nwn;
SET NAMES cp1250;



#-- re-create table
drop table if exists acs_defs_m;			#-- meta definitions
drop table if exists acs_defs_mt;			#-- meta definitions with absolute address (temp only)
drop table if exists acs_defs_mtx;			#-- meta definitions with existing nodes (temp only)
drop table if exists acs_rels_m;			#-- meta relations
drop table if exists acs_units;				#-- recipe sizing units
drop table if exists acs_cats;				#-- recipe categories
drop table if exists acs_defs;				#-- recipe definitions
drop table if exists acs_rels;				#-- recipe relations
drop table if exists cargo_defs;			#-- cargo definitions
drop table if exists cargo_items;			#-- cargo items list
drop table if exists board_messages;		#-- board messages
drop table if exists events_defs;			#-- event definitions
drop table if exists events_waves;			#-- event wave definitions
drop table if exists events_rewards;		#-- event rewards
drop table if exists tis_otypes;			#-- types of interactive objects
drop table if exists tis_osubs;				#-- subtypes of interactive objects
drop table if exists tis_atypes;			#-- types of interactions
drop table if exists tis_asubs;				#-- subtypes of interactions
drop table if exists tis_settings;			#-- static settings of interactions/object subtypes
drop table if exists tis_spawndef;			#-- spawn definitions
drop table if exists tis_spawnlist;			#-- spawn list
drop table if exists teachers_defs;			#-- teacher definitions
drop table if exists teachers_reqs;			#-- teacher requirements
drop table if exists xchange_defs;			#-- exchange store definitions
drop table if exists xchange_items;			#-- exchange store items
drop table if exists xstore_defs;			#-- store definitions
drop table if exists xstore_items;			#-- store items
drop table if exists fds_nutrition;			#-- food/drink nutrition values
drop table if exists lts_treasure;			#-- treasure object definitions
drop table if exists lpd_convdef;			#-- conversation definitions
drop table if exists lpd_convstruct;		#-- conversation structures


/*log
select '> Tables deleted' as "Log";#--*/



create table
acs_defs_m
(
	 id int primary key
	,modweight float(10,3) null
	,moddurab float(10,3) null
) character set utf8 collate utf8_unicode_ci;



create table
acs_defs_mt
(
	 id int primary key
	,modweight float(10,3) null
	,moddurab float(10,3) null
	,addraw text default null
	,updt int default -1
) character set utf8 collate utf8_unicode_ci;



create table
acs_defs_mtx
(
	 id int default -1
	,addraw text default null
	,curr int default -1
) character set utf8 collate utf8_unicode_ci;



create table
acs_rels_m
(
	 rid int default -1
	,rid_qty int default -1
	,rid_group int default null
	,rid_address text default null
	,rid_addraw text default null
	#--,primary key (rid,rid_group)
) character set utf8 collate utf8_unicode_ci;



create table
acs_cats
(
	 id int default -1
	,label varchar(100) null
	,name varchar(250) character set utf8 null
) character set utf8 collate utf8_unicode_ci;



create table
acs_units
(
	 id int default -1
	,label varchar(100) null
	,base1u varchar(10) null
	,base1k varchar(10) null
	,base1m varchar(10) null
) character set utf8 collate utf8_unicode_ci;



create table
acs_defs
(
	 id int default -1
	,cid int default -1
	,clabel varchar(100) null
	,label varchar(100) null
	,display varchar(250) character set utf8 null
	,resref varchar(16) null
	,qty int null
	,modweight float(10,3) null
	,moddurab float(10,3) null
	,uid int null
	,ulabel varchar(30) null
) character set utf8 collate utf8_unicode_ci;



create table
acs_rels
(
	 id int default -1
	,label varchar(100) null
	,rid int default -1
	,label_src varchar(100) null
	,rid_src int default -1
	,qty_src int null
) character set utf8 collate utf8_unicode_ci;



create table
cargo_defs
(
	 id int default -1
	,label varchar(100) null
	,display varchar(250) null
	,tag varchar(32) null
	,dur int null
	,tGreet varchar(255) null
	,tInfoS varchar(255) null
	,tInfoA varchar(255) null
	,tCargoN varchar(255) null
	,tCargoS varchar(255) null
	,tCargoI varchar(255) null
	,tCargoD varchar(255) null
) character set utf8 collate utf8_unicode_ci;



create table
cargo_items
(
	 id int default -1
	,label varchar(100) null
	,cid int null
	,clabel varchar(100) null
	,rid int null
	,rqty int null
	,rspec1 int null
	,rspec2 int null
	,rspec3 int null
	,rspec4 int null
	,rspec5 int null
) character set utf8 collate utf8_unicode_ci;



create table
board_messages
(
	 id int default -1
	,label varchar(100) null
	,tag varchar(32) null
	,msg varchar(255) null
) character set utf8 collate utf8_unicode_ci;



create table
events_defs
(
	 id int default -1
	,label varchar(100) null
	,tag varchar(32) null
	,style int default -1
	,area varchar(32) null
	,pause int null
	,waveln int null
	,pcmin int null
	,pcmax int null
	,tNone varchar(255) null
	,tProg varchar(255) null
	,tDetail varchar(255) null
	,tDone varchar(255) null
) character set utf8 collate utf8_unicode_ci;



create table
events_waves
(
	 id int default -1
	,label varchar(100) null
	,eid int null
	,wave int null
	,resref varchar(16) null
	,amount int null
	,sound varchar(16) null
	,tWave varchar(100) null
) character set utf8 collate utf8_unicode_ci;



create table
events_rewards
(
	 id int default -1
	,label varchar(100) null
	,eid int null
	,rid int null
	,amount int null
	,rspec1 int null
	,rspec2 int null
	,rspec3 int null
	,rspec4 int null
	,rspec5 int null
) character set utf8 collate utf8_unicode_ci;



create table
tis_otypes
(
	 id int default -1
	,label varchar(100) null
	,display varchar(100) null
	,tech varchar(16) null
	,vfx int null
	,vfx_sc int null
) character set utf8 collate utf8_unicode_ci;



create table
tis_osubs
(
	 id int default -1
	,label varchar(100) null
	,display varchar(100) null
	,tid int null
	,tlabel varchar(100) null
	,tag varchar(32) null
) character set utf8 collate utf8_unicode_ci;



create table
tis_atypes
(
	 id int default -1
	,label varchar(100) null
	,display varchar(100) null
	,tech varchar(16) null
) character set utf8 collate utf8_unicode_ci;



create table
tis_asubs
(
	 id int default -1
	,label varchar(100) null
	,tid int null
	,tlabel varchar(100) null
	,iid int null
	,ilabel varchar(100) null
	,display varchar(100) null
	,vfx int null
) character set utf8 collate utf8_unicode_ci;



create table
tis_settings
(
	 id int default -1
	,label varchar(100) null
	,osid int null
	,oslabel varchar(100) null
	,asid int null
	,aslabel varchar(100) null
	,post varchar(32) null
	,typ varchar(1) null
	,val varchar(255) null
) character set utf8 collate utf8_unicode_ci;



create table
tis_spawndef
(
	 id int default -1
	,label varchar(100) null
	,display varchar(100) null
	,name varchar(32) null
	,spawn int null
	,respawn int null
	,actstart int null
	,actend int null
) character set utf8 collate utf8_unicode_ci;



create table
tis_spawnlist
(
	 id int default -1
	,sid int null
	,otype varchar(1) null
	,resref varchar(16) null
) character set utf8 collate utf8_unicode_ci;



create table
teachers_defs
(
	 id int default -1
	,label varchar(100) null
	,tag varchar(32) null
	,vname varchar(32) null
	,vvalue int null
	,mrequest varchar(255) null
	,trequire varchar(255) null
	,tteach varchar(255) null
) character set utf8 collate utf8_unicode_ci;



create table
teachers_reqs
(
	 trid int default -1
	,tdlabel varchar(100) null
	,tdid int null
	,rlabel varchar(100) null
	,rid int null
	,rqty int null
	,rspec1 int null
	,rspec2 int null
	,rspec3 int null
	,rspec4 int null
	,rspec5 int null
) character set utf8 collate utf8_unicode_ci;



create table
xchange_defs
(
	 id int default -1
	,label varchar(100) null
	,menu varchar(100) null
	,tag varchar(32) null
	,info varchar(100) null
) character set utf8 collate utf8_unicode_ci;



create table
xchange_items
(
	 id int default -1
	,label varchar(100) null
	,xid int null
	,rlabel varchar(100) null
	,rid int null
	,rqty int null
	,rspec1 int null
	,rspec2 int null
	,rspec3 int null
	,rspec4 int null
	,rspec5 int null
) character set utf8 collate utf8_unicode_ci;



create table
fds_nutrition
(
	 id int default -1
	,label varchar(100) null
	,tag varchar(32) null
	,food int null
	,liquid int null
) character set utf8 collate utf8_unicode_ci;



create table
xstore_defs
(
	 id int default -1
	,label varchar(100) null
	,tag varchar(32) null
) character set utf8 collate utf8_unicode_ci;



create table
xstore_items
(
	 id int default -1
	,label varchar(100) null
	,xid int null
	,rlabel varchar(100) null
	,rid int null
	,rqty int null
	,rspec1 int null
	,rspec2 int null
	,rspec3 int null
	,rspec4 int null
	,rspec5 int null
	,cost int null
) character set utf8 collate utf8_unicode_ci;



create table
lts_treasure
(
	 id int default -1
	,label varchar(100) null
	,tag varchar(32) null
	,valmin int null
	,valmax int null
) character set utf8 collate utf8_unicode_ci;



create table
lpd_convdef
(
	 id int default -1
	,label varchar(100) null
	,tag varchar(32) null
) character set utf8 collate utf8_unicode_ci;




create table
lpd_convstruct
(
	 id int default -1
	,npc int null
	,label varchar(100) null
	,node varchar(10) null
	,child varchar(10) null
	,keyword varchar(32) null
	,text varchar(255) null
) character set utf8 collate utf8_unicode_ci;

/*log
select '> Tables created' as "Log";#--*/



#--load data - categories
load data local infile 'sql/il2_md_acs_cats.txt'
into table acs_cats
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	name = if(name = '', NULL, name)
;



#--load data - sizing units
load data local infile 'sql/il2_md_acs_units.txt'
into table acs_units
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	base1u = if(base1u = '', NULL, base1u),
	base1k = if(base1k = '', NULL, base1k),
	base1m = if(base1m = '', NULL, base1m)
;



#--load data - definitions
load data local infile 'sql/il2_md_acs_defs.txt'
into table acs_defs
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	cid = if(cid = '', NULL, cid),
	clabel = if(clabel = '', NULL, clabel),
	label = if(label = '', NULL, label),
	display = if(display = '', NULL, display),
	resref = if(resref = '', NULL, resref),
	qty = if(qty = '', NULL, qty),
	modweight = if(modweight = '', NULL, modweight),
	moddurab = if(moddurab = '', NULL, moddurab),
	uid = if(uid = '', NULL, uid),
	ulabel = if(ulabel = '', NULL, ulabel)
;



#--load data - relations
load data local infile 'sql/il2_md_acs_rels.txt'
into table acs_rels
	fields terminated by '|^|' enclosed by ''
	lines terminated by '\r\n'
ignore 1 lines
set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	rid = if(rid = '', NULL, rid),
	label_src = if(label_src = '', NULL, label_src),
	rid_src = if(rid_src = '', NULL, rid_src),
	qty_src = if(qty_src = '', NULL, qty_src)
;



#--load data - cargo defs
load data local infile 'sql/il2_md_cargo_defs.txt'
into table cargo_defs
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	display = if(display = '', NULL, display),
	tag = if(tag = '', NULL, tag),
	dur = if(dur = '', NULL, dur),
	tGreet = if(tGreet = '', NULL, tGreet),
	tInfoS = if(tInfoS = '', NULL, tInfoS),
	tInfoA = if(tInfoA = '', NULL, tInfoA),
	tCargoN = if(tCargoN = '', NULL, tCargoN),
	tCargoS = if(tCargoS = '', NULL, tCargoS),
	tCargoI = if(tCargoI = '', NULL, tCargoI),
	tCargoD = if(tCargoD = '', NULL, tCargoD)
;


#--load data - cargo items
load data local infile 'sql/il2_md_cargo_items.txt'
into table cargo_items
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	cid = if(cid = '', NULL, cid),
	clabel = if(clabel = '', NULL, clabel),
	rid = if(rid = '', NULL, rid),
	rqty = if(rqty = '', NULL, rqty),
	rspec1 = if(rspec1 = '', NULL, rspec1),
	rspec2 = if(rspec2 = '', NULL, rspec2),
	rspec3 = if(rspec3 = '', NULL, rspec3),
	rspec4 = if(rspec4 = '', NULL, rspec4),
	rspec5 = if(rspec4 = '', NULL, rspec5)
;



#--load data - board messages
load data local infile 'sql/il2_md_board_messages.txt'
into table board_messages
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	tag = if(tag = '', NULL, tag),
	msg = if(msg = '', NULL, msg)
;



#--load data - events definitions
load data local infile 'sql/il2_md_events_defs.txt'
into table events_defs
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	tag = if(tag = '', NULL, tag),
	style = if(style = '', NULL, style),
	area = if(area = '', NULL, area),
	pause = if(pause = '', NULL, pause),
	pcmin = if(pcmin = '', NULL, pcmin),
	pcmax = if(pcmax = '', NULL, pcmax),
	tNone = if(tNone = '', NULL, tNone),
	tProg = if(tProg = '', NULL, tProg),
	tDetail = if(tDetail = '', NULL, tDetail),
	tDone = if(tDone = '', NULL, tDone)
;



#--load data - events waves
load data local infile 'sql/il2_md_events_waves.txt'
into table events_waves
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	eid = if(eid = '', NULL, eid),
	wave = if(wave = '', NULL, wave),
	resref = if(resref = '', NULL, resref),
	amount = if(amount = '', NULL, amount),
	sound = if(sound = '', NULL, sound),
	tWave = if(tWave = '', NULL, tWave)
;



#--load data - events rewards
load data local infile 'sql/il2_md_events_rewards.txt'
into table events_rewards
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	eid = if(eid = '', NULL, eid),
	rid = if(rid = '', NULL, rid),
	amount = if(amount = '', NULL, amount),
	rspec1 = if(rspec1 = '', NULL, rspec1),
	rspec2 = if(rspec2 = '', NULL, rspec2),
	rspec3 = if(rspec3 = '', NULL, rspec3),
	rspec4 = if(rspec4 = '', NULL, rspec4),
	rspec5 = if(rspec5 = '', NULL, rspec5)
;



#--load data - interactive object types
load data local infile 'sql/il2_md_tis_otypes.txt'
into table tis_otypes
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	display = if(display = '', NULL, display),
	tech = if(tech = '', NULL, tech),
	vfx = if(vfx = '', NULL, vfx),
	vfx_sc = if(vfx_sc = '', NULL, vfx_sc)
;



#--load data - interactive object subtypes
load data local infile 'sql/il2_md_tis_osubs.txt'
into table tis_osubs
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	display = if(display = '', NULL, display),
	tid = if(tid = '', NULL, tid),
	tlabel = if(tlabel = '', NULL, tlabel),
	tag = if(tag = '', NULL, tag)
;



#--load data - interactions types
load data local infile 'sql/il2_md_tis_atypes.txt'
into table tis_atypes
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	display = if(display = '', NULL, display),
	tech = if(tech = '', NULL, tech)
;



#--load data - interactions subtypes
load data local infile 'sql/il2_md_tis_asubs.txt'
into table tis_asubs
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	tid = if(tid = '', NULL, tid),
	tlabel = if(tlabel = '', NULL, tlabel),
	iid = if(iid = '', NULL, iid),
	ilabel = if(ilabel = '', NULL, ilabel),
	display = if(display = '', NULL, display),
	vfx = if(vfx = '', NULL, vfx)
;



#--load data - static settings of interactions/object subtypes
load data local infile 'sql/il2_md_tis_settings.txt'
into table tis_settings
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	osid = if(osid = '', NULL, osid),
	oslabel = if(oslabel = '', NULL, oslabel),
	asid = if(asid = '', NULL, asid),
	aslabel = if(aslabel = '', NULL, aslabel),
	post = if(post = '', NULL, post),
	typ = if(typ = '', NULL, typ),
	val = if(val = '', NULL, val)
;



#--load data - tis spawn definitions
load data local infile 'sql/il2_md_tis_spawndef.txt'
into table tis_spawndef
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	display = if(display = '', NULL, display),
	name = if(name = '', NULL, name),
	spawn = if(spawn = '', NULL, spawn),
	respawn = if(respawn = '', NULL, respawn),
	actstart = if(actstart = '', NULL, actstart),
	actend = if(actend = '', NULL, actend)
;



#--load data - tis spawn list
load data local infile 'sql/il2_md_tis_spawnlist.txt'
into table tis_spawnlist
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	sid = if(sid = '', NULL, sid),
	otype = if(otype = '', NULL, otype),
	resref = if(resref = '', NULL, resref)
;



#--load data - teacher definitions
load data local infile 'sql/il2_md_teachers_defs.txt'
into table teachers_defs
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	tag = if(tag = '', NULL, tag),
	vname = if(vname = '', NULL, vname),
	vvalue = if(vvalue = '', NULL, vvalue),
	mrequest = if(mrequest = '', NULL, mrequest),
	trequire = if(trequire = '', NULL, trequire),
	tteach = if(tteach = '', NULL, tteach)
;



#--load data - teacher requirements
load data local infile 'sql/il2_md_teachers_reqs.txt'
into table teachers_reqs
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	trid = if(trid = '', NULL, trid),
	tdlabel = if(tdlabel = '', NULL, tdlabel),
	tdid = if(tdid = '', NULL, tdid),
	rlabel = if(rlabel = '', NULL, rlabel),
	rid = if(rid = '', NULL, rid),
	rqty = if(rqty = '', NULL, rqty),
	rspec1 = if(rspec1 = '', NULL, rspec1),
	rspec2 = if(rspec2 = '', NULL, rspec2),
	rspec3 = if(rspec3 = '', NULL, rspec3),
	rspec4 = if(rspec4 = '', NULL, rspec4),
	rspec5 = if(rspec5 = '', NULL, rspec5)
;



#--load data - exchange store definitions
load data local infile 'sql/il2_md_xchange_defs.txt'
into table xchange_defs
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	menu = if(menu = '', NULL, menu),
	tag = if(tag = '', NULL, tag),
	info = if(info = '', NULL, info)
;



#--load data - exchange store items
load data local infile 'sql/il2_md_xchange_items.txt'
into table xchange_items
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	xid = if(xid = '', NULL, xid),
	rlabel = if(rlabel = '', NULL, rlabel),
	rid = if(rid = '', NULL, rid),
	rqty = if(rqty = '', NULL, rqty),
	rspec1 = if(rspec1 = '', NULL, rspec1),
	rspec2 = if(rspec2 = '', NULL, rspec2),
	rspec3 = if(rspec3 = '', NULL, rspec3),
	rspec4 = if(rspec4 = '', NULL, rspec4),
	rspec5 = if(rspec5 = '', NULL, rspec5)
;



#--load data - store definitions
load data local infile 'sql/il2_md_xstore_defs.txt'
into table xstore_defs
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	tag = if(tag = '', NULL, tag)
;



#--load data - store items
load data local infile 'sql/il2_md_xstore_items.txt'
into table xstore_items
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	xid = if(xid = '', NULL, xid),
	rlabel = if(rlabel = '', NULL, rlabel),
	rid = if(rid = '', NULL, rid),
	rqty = if(rqty = '', NULL, rqty),
	rspec1 = if(rspec1 = '', NULL, rspec1),
	rspec2 = if(rspec2 = '', NULL, rspec2),
	rspec3 = if(rspec3 = '', NULL, rspec3),
	rspec4 = if(rspec4 = '', NULL, rspec4),
	rspec5 = if(rspec5 = '', NULL, rspec5),
	cost = if(cost = '', NULL, cost)
;



#--load data - food/drink nutrition values
load data local infile 'sql/il2_md_fds_nutrition.txt'
into table fds_nutrition
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	tag = if(tag = '', NULL, tag),
	food = if(food = '', NULL, food),
	liquid = if(liquid = '', NULL, liquid)
;



#--load data - treasure object definitions
load data local infile 'sql/il2_md_lts_treasure.txt'
into table lts_treasure
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	tag = if(tag = '', NULL, tag),
	valmin = if(valmin = '', NULL, valmin),
	valmax = if(valmax = '', NULL, valmax)
;



#--load data - conversation definitions
load data local infile 'sql/il2_md_lpd_convdef.txt'
into table lpd_convdef
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	label = if(label = '', NULL, label),
	tag = if(tag = '', NULL, tag)
;



#--load data - conversation structures
load data local infile 'sql/il2_md_lpd_convstruct.txt'
into table lpd_convstruct
	fields terminated by '|^|' enclosed by '' escaped by ''
	lines terminated by '\r\n'
ignore 1 lines

set
	id = if(id = '', NULL, id),
	npc = if(npc = '', NULL, npc),
	label = if(label = '', NULL, label),
	node = if(node = '', NULL, node),
	child = if(child = '', NULL, child),
	keyword = if(keyword = '', NULL, keyword),
	text = if(text = '', NULL, text)
;



#--add newly added cargos to complete table (flag as incomplete and calculate expiration date (game_date + cargo_duration_days))

insert into cargo_complete
select distinct d.id#--, adddate(s.gamedate, interval d.dur day), 

    ,cast(concat(
        mid(s.gamedate, 1, 4) + truncate((mid(s.gamedate, 6, 2) + truncate((mid(s.gamedate, 9, 2) + d.dur) / 28, 0)) / 9, 0), '-',
        (mid(s.gamedate, 6, 2) + truncate((mid(s.gamedate, 9, 2) + d.dur) / 28, 0)) % 9, '-',
        (mid(s.gamedate, 9, 2) + d.dur) % 28, mid(s.gamedate, 11, length(s.gamedate))
    ) as datetime) as NewDate

    ,0, now()
from cargo_defs as d
left join cargo_complete as c on d.id = c.CargoID
inner join sys_settings as s on 1 = 1
inner join cargo_items as i on d.id = i.cid
where c.CargoID is null and d.dur is not null and i.cid is not null
;



drop procedure if exists acs_metadata_p;

delimiter $$
create procedure acs_metadata_p()

begin
	#-- empty the metadata tables
	truncate table acs_rels_m;
	truncate table acs_defs_m;
	truncate table acs_defs_mt;
	truncate table acs_defs_mtx;

	
	
	#--------------------------
	#-- metadata - definitions
	#--------------------------
	
	
	
	#-- start A: non-grouped static recipes
	insert into
		acs_defs_mt
	select distinct
		 rx.rid
		,coalesce(dx.modweight, 0) as modweight
		,coalesce(dx.moddurab, 0) as moddurab
		,null as addraw
		,1 as updt
	from
		acs_defs as d
		
		left join acs_rels as r on
		r.rid = d.id
		
		join acs_rels as rx on
		rx.rid = r.rid_src
		
		left join acs_defs as dx on
		rx.rid = dx.id
	where
		rx.rid_src is null and
		d.qty is not null and
		d.label is not null
	;
	
	
	
	#-- start B: dynamic recipes that consists of groups ( above groups )
	insert into
		acs_defs_mt
	select distinct
		 r.rid
		,coalesce(d.modweight, 0) as modweight
		,coalesce(d.moddurab, 0) as moddurab
		,null as addraw
		,1 as updt
	from
		acs_defs as d
		
		left join acs_rels as r on
		r.rid = d.id
		
		left join acs_defs as dg on
		dg.id = r.rid_src
	where
		r.rid_src is not null and
		dg.qty is null and
		d.label is not null
	;
	
	
	
	insert into
		acs_defs_mtx
	select
		id
		,null as addraw
		,0 as curr
	from
		acs_defs_mt
	;
	
	
	
	#-- loop within the hierarchy upwards until highest level
	set @cnt = row_count();

	

	while @cnt > 0
	do
		insert into
			acs_defs_mtx
		select
			 r.rid
			,concat(m.id, ';', coalesce(m.addraw, '')) as addraw
			,1 as curr
		from
			acs_rels as r
			
			inner join acs_defs_mt as m on 
			m.id = r.rid_src
			
			left join acs_defs as dr on
			dr.id = r.rid
			
			left join acs_defs_mtx as x on
			x.id = r.rid and
			x.addraw = concat(m.id, ';', coalesce(m.addraw, ''))
		where
			r.rid != -1 and
			x.id is null
		;
		


		#-- insert the next level of the group-based hierarchy
		insert ignore into
			acs_defs_mt
		select distinct
			 r.rid as rid
			,sum(r.qty_src * m.modweight) as modweight
			,sum(r.qty_src * m.moddurab) as moddurab
			,concat(m.id, ';', coalesce(m.addraw, '')) as addraw
			,0 as upd
		from
			acs_rels as r
			
			inner join acs_defs_mt as m on 
			m.id = r.rid_src
			
			left join acs_defs as dr on
			dr.id = r.rid
			
			left join acs_defs_mtx as x on
			x.id = r.rid and
			x.addraw = concat(m.id, ';', coalesce(m.addraw, ''))
		where
			r.rid != -1 and
			(x.id is null or x.curr = 1)
		group by
			r.rid 
		order by
			 r.rid
			,m.id
		on duplicate key update
			 modweight = modweight + values(modweight)
			,moddurab = moddurab + values(moddurab)
		;
		
		
		
		update
			acs_defs_mt as m
			
			left join acs_defs as dr on
			dr.id = m.id
		set
			 m.modweight = m.modweight + coalesce(dr.modweight, 0)
			,m.moddurab = m.moddurab + coalesce(dr.moddurab, 0)
			,m.updt = 1
		where
			updt = 0
		;
		

		
		update
			acs_defs_mtx
		set
			curr = 0
		where
			curr = 1
		;
		

		
		#-- get the count of rows affected
		set @cnt = row_count();
	end while;
	
	
	
	insert into
		acs_defs_m
	select
		 id
		,modweight
		,moddurab
	from
		acs_defs_mt
	;
	
	
	
	drop table if exists acs_defs_mt;
	drop table if exists acs_defs_mtx;
	
	
	
    delete m.*
    from acs_defs_m as m left join acs_defs as d on m.id = d.id
    where d.qty is null;
    
    delete m.*
    from acs_defs_m as m left join  acs_rels as r on m.id = r.rid
    where r.rid_src is null;
        
        
    insert into
        acs_defs_m
    select
         d.id
        ,coalesce(d.modweight, 0)
        ,coalesce(d.moddurab, 0)
     from
        acs_defs as d
        
        left join acs_rels as r on d.id = r.rid
    where
        r.rid_src is null and
        d.label is not null
	;
	
	
	
	
	#------------------------
	#-- metadata - relations
	#------------------------
	
	
	
	#-- groups
	insert into
		acs_rels_m
	select distinct
		 r.rid
		,null
		,-1
		,null
		,null
	from
		acs_defs as d
		
		left join acs_rels as r on
		r.rid = d.id
	where
		d.qty is null and
		d.label is not null
	;


/*
	#-- static rids that are grouped
	insert into
		acs_rels_m
	select
		 null
		,rid_group
		,null
	from
		acs_rels_m
	;

	
	
	#-- missing rids from metadata
	select 
		r.* 
	from 
		acs_rels_params_v v 
		
		right join acs_rels r on 
		r.rid = v.rid and 
		r.rid_src = v.rid_group 
	where 
		v.rid is null and 
		r.rid is not null
	;

	
	
	#-- next lev with labels
	select distinct
		 null
		,r.rid
		,dr.label
		,m.rid
		,dm.label
	from
		acs_rels as r
		
		inner join acs_rels_m as m on 
		m.rid = r.rid_src
		
		left join acs_rels_m as x on
		x.rid = r.rid and
		x.rid_group = m.rid
		
		left join acs_defs as dr on
		dr.id = r.rid
		
		left join acs_defs as dm on
		dm.id = m.rid
	where
		r.rid != -1 and
		x.rid is null
	order by
		 r.rid
		,m.rid
	;
	
	
	
	
*/



	#-- loop within the hierarchy upwards until highest level
	set @cnt = row_count();

	

	while @cnt > 0
	do
		#-- insert the next level of the group-based hierarchy
		insert into
			acs_rels_m
		select distinct
			 r.rid as rid
			,r.qty_src * coalesce(m.rid_qty, 1) as rid_qty
			,case when mg.rid is null then m.rid else mg.rid end as rid_group
			,concat(dm.display, if(m.rid_address is null,'',' >'), concat(' ', coalesce(m.rid_address, ''))) as addr
			,concat(m.rid, ';', coalesce(m.rid_addraw, '')) as addraw
		from
			acs_rels as r
			
			inner join acs_rels_m as m on 
			m.rid = r.rid_src
			
			left join acs_rels_m as mg on
			mg.rid = m.rid_group
		
			left join acs_defs as dm on
			dm.id = m.rid

			left join acs_defs as dmg on
			dmg.id = case when mg.rid is null then m.rid else mg.rid end
			
			left join acs_rels_m as x on
			x.rid = r.rid  and
			x.rid_addraw = concat(m.rid, ';', coalesce(m.rid_addraw, ''))
		where
			r.rid != -1 and
			x.rid is null
		order by
			 r.rid
			,m.rid
		;
		
		
		
		#-- get the count of rows affected
		set @cnt = row_count();
	end while;
	
	insert into
		acs_rels_m
	select
		rid,
		1,
		rid_src,
		display,
		concat(rid_src, ';')
	from
		acs_rels as r
		
		left join acs_defs as d on
		d.id = r.rid_src
	where
		rid = 187
	;

	update
		acs_rels_m as m
		
		left join acs_defs as d on
		d.id = m.rid
	set
		rid_address = concat(d.display, ' > ', rid_address)
	;

end$$
delimiter ;

/*log
select '> Procedure created' as "Log";#--*/

call acs_metadata_p;

/*log
select '> Procedure executed' as "Log";#--*/


delimiter $$
drop view if exists acs_rels_m_v$$
create view acs_rels_m_v as
	select
		#-- m.rid as rid
		#--,d.label as label
		concat(m.rid, ': ', d.label) as rid
		#--,m.rid_qty as rid_qty
		#--,m.rid_group as rid_group
		#--,x.label as label_group
		,concat(m.rid_group, ': ', x.label, ' x', m.rid_qty, '') as "group"
		,m.rid_address as rid_address
		,m.rid_addraw as rid_addraw
	from
		acs_rels_m as m
		
		left join acs_defs as d on
		d.id = m.rid
		
		left join acs_defs as x on
		x.id = m.rid_group
	order by
		1, 2, 4
	$$



drop view if exists acs_defs_m_v$$
create view acs_defs_m_v as
	select
		concat(m.id, ': ', d.label) as rid
		,m.modweight
		,m.moddurab
	from
		acs_defs_m as m
		
		left join acs_defs as d on
		d.id = m.id
	order by
		1
	$$
	
delimiter ;
#select * from acs_rels_m_v;
#select * from acs_defs_m_v;
