﻿#|^|Label|^|oSub|^|oSub_label|^|aSub|^|aSub_label|^|Postfix|^|Type|^|Value
100|^|nTIS_SET_OS_100_AS_100_RECIPE|^|100|^|.plant_roots|^|100|^|.plant_acquire|^|RECIPE|^|n|^|127
101|^|nTIS_SET_OS_100_LEVEL|^|100|^|.plant_roots|^|-1|^||^|LEVEL|^|n|^|1
102|^|nTIS_SET_AS_100_QTY|^|-1|^||^|-1|^||^|AS_100_QTY|^|n|^|10
103|^|sTIS_SET_OS_100_AS_126_DESC|^|100|^|.plant_roots|^|126|^|.generic_examine|^|DESC|^|s|^|After a while you can see these are edible plant roots
104|^|nTIS_SET_OS_101_AS_100_RECIPE|^|101|^|.plant_fruits|^|100|^|.plant_acquire|^|RECIPE|^|n|^|128
105|^|nTIS_SET_OS_101_LEVEL|^|101|^|.plant_fruits|^|-1|^||^|LEVEL|^|n|^|1
106|^|sTIS_SET_OS_101_AS_126_DESC|^|101|^|.plant_fruits|^|126|^|.generic_examine|^|DESC|^|s|^|The typical smell suits to common forest berries
107|^|nTIS_SET_OS_102_AS_100_RECIPE|^|102|^|.plant_foggy|^|100|^|.plant_acquire|^|RECIPE|^|n|^|123
108|^|nTIS_SET_OS_102_LEVEL|^|102|^|.plant_foggy|^|-1|^||^|LEVEL|^|n|^|1
109|^|sTIS_SET_OS_102_AS_126_DESC|^|102|^|.plant_foggy|^|126|^|.generic_examine|^|DESC|^|s|^|You are able to feel wet leafs and see a dim fog stepping out of the plant's flowers
110|^|nTIS_SET_OS_103_AS_100_RECIPE|^|103|^|.plant_gravy|^|100|^|.plant_acquire|^|RECIPE|^|n|^|122
111|^|nTIS_SET_OS_103_LEVEL|^|103|^|.plant_gravy|^|-1|^||^|LEVEL|^|n|^|1
112|^|sTIS_SET_OS_103_AS_126_DESC|^|103|^|.plant_gravy|^|126|^|.generic_examine|^|DESC|^|s|^|By simply considering the typical smell of this plant you can say that it was growing near some decaying animal
113|^|nTIS_SET_OS_104_AS_100_RECIPE|^|104|^|.mushroom_cheesy|^|100|^|.plant_acquire|^|RECIPE|^|n|^|124
114|^|nTIS_SET_OS_104_LEVEL|^|104|^|.mushroom_cheesy|^|-1|^||^|LEVEL|^|n|^|1
115|^|nTIS_SET_AS_101_QTY|^|-1|^||^|-1|^||^|AS_101_QTY|^|n|^|25
116|^|sTIS_SET_OS_104_AS_126_DESC|^|104|^|.mushroom_cheesy|^|126|^|.generic_examine|^|DESC|^|s|^|The mushroom has a cheese appearance and grows on the tree trunk
117|^|nTIS_SET_OS_105_AS_100_RECIPE|^|105|^|.mushroom_crawl|^|100|^|.plant_acquire|^|RECIPE|^|n|^|125
118|^|nTIS_SET_OS_105_LEVEL|^|105|^|.mushroom_crawl|^|-1|^||^|LEVEL|^|n|^|1
119|^|sTIS_SET_OS_105_AS_126_DESC|^|105|^|.mushroom_crawl|^|126|^|.generic_examine|^|DESC|^|s|^|Looks like a bigger grass with long crawling roots
120|^|sTIS_SET_AT_101_DEFST_TREE|^|-1|^||^|-1|^||^|AT_101_DEFST_TREE|^|s|^|FELL
121|^|sTIS_SET_AT_101_DEFST_ROCK|^|-1|^||^|-1|^||^|AT_101_DEFST_ROCK|^|s|^|MINE
122|^|sTIS_SET_AT_101_NAME_FELL|^|-1|^||^|-1|^||^|AT_101_NAME_FELL|^|s|^|Fell the tree
123|^|sTIS_SET_AT_101_NAME_FELL_P|^|-1|^||^|-1|^||^|AT_101_NAME_FELL_P|^|s|^|tree felling
124|^|sTIS_SET_AT_101_NAME_CUT|^|-1|^||^|-1|^||^|AT_101_NAME_CUT|^|s|^|Cut the wood
125|^|sTIS_SET_AT_101_NAME_CUT_P|^|-1|^||^|-1|^||^|AT_101_NAME_CUT_P|^|s|^|wood cutting
126|^|sTIS_SET_AT_101_NAME_MINE|^|-1|^||^|-1|^||^|AT_101_NAME_MINE|^|s|^|Mine the rock
127|^|sTIS_SET_AT_101_NAME_MINE_P|^|-1|^||^|-1|^||^|AT_101_NAME_MINE_P|^|s|^|rock mining
128|^|sTIS_SET_AT_101_NAME_CRUSH|^|-1|^||^|-1|^||^|AT_101_NAME_CRUSH|^|s|^|Crush the rock
129|^|sTIS_SET_AT_101_NAME_CRUSH_P|^|-1|^||^|-1|^||^|AT_101_NAME_CRUSH_P|^|s|^|rock crushing
130|^|sTIS_SET_AT_101_TAG_TREE|^|-1|^||^|-1|^||^|AT_101_TAG_TREE|^|s|^|VG_P_UC_TREESH01
131|^|sTIS_SET_AT_101_TAG_ROCK|^|-1|^||^|-1|^||^|AT_101_TAG_ROCK|^|s|^|VG_P_UC_STONES01
132|^|nTIS_SET_OS_108_AS_109_AMOUNT|^|108|^|.tree_beech|^|109|^|.tree_mine|^|AMOUNT|^|n|^|5
133|^|nTIS_SET_OS_108_AS_109_RECIPE|^|108|^|.tree_beech|^|109|^|.tree_mine|^|RECIPE|^|n|^|120
134|^|nTIS_SET_OS_108_LEVEL|^|108|^|.tree_beech|^|-1|^||^|LEVEL|^|n|^|1
135|^|nTIS_SET_AS_109_QTY|^|-1|^||^|-1|^||^|AS_109_QTY|^|n|^|2000
136|^|sTIS_SET_OS_108_AS_126_DESC|^|108|^|.tree_beech|^|126|^|.generic_examine|^|DESC|^|s|^|Didn't take long and you were able to spot the typical egg-eliptic shape of the crown of the tree and its leafs
137|^|nTIS_SET_OS_109_AS_109_AMOUNT|^|109|^|.tree_oak|^|109|^|.tree_mine|^|AMOUNT|^|n|^|5
138|^|nTIS_SET_OS_109_AS_109_RECIPE|^|109|^|.tree_oak|^|109|^|.tree_mine|^|RECIPE|^|n|^|121
139|^|nTIS_SET_OS_109_LEVEL|^|109|^|.tree_oak|^|-1|^||^|LEVEL|^|n|^|1
140|^|sTIS_SET_OS_109_AS_126_DESC|^|109|^|.tree_oak|^|126|^|.generic_examine|^|DESC|^|s|^|You have spot thin wavy leafs with an egg-facing-down shape with few acorns nearby.
141|^|nTIS_SET_OS_106_AS_108_AMOUNT|^|106|^|.rock_copper|^|108|^|.rock_mine|^|AMOUNT|^|n|^|5
142|^|nTIS_SET_OS_106_AS_108_RECIPE|^|106|^|.rock_copper|^|108|^|.rock_mine|^|RECIPE|^|n|^|118
143|^|nTIS_SET_OS_106_LEVEL|^|106|^|.rock_copper|^|-1|^||^|LEVEL|^|n|^|1
144|^|nTIS_SET_AS_108_QTY|^|-1|^||^|-1|^||^|AS_108_QTY|^|n|^|2000
145|^|sTIS_SET_OS_106_AS_126_DESC|^|106|^|.rock_copper|^|126|^|.generic_examine|^|DESC|^|s|^|This rock has quite many dark red iron veins in it
146|^|nTIS_SET_OS_107_AS_108_AMOUNT|^|107|^|.rock_gold|^|108|^|.rock_mine|^|AMOUNT|^|n|^|5
147|^|nTIS_SET_OS_107_AS_108_RECIPE|^|107|^|.rock_gold|^|108|^|.rock_mine|^|RECIPE|^|n|^|119
148|^|nTIS_SET_OS_107_LEVEL|^|107|^|.rock_gold|^|-1|^||^|LEVEL|^|n|^|1
149|^|sTIS_SET_OS_107_AS_126_DESC|^|107|^|.rock_gold|^|126|^|.generic_examine|^|DESC|^|s|^|Very little but certain amount of golden grains can be found inside this rock
150|^|nTIS_SET_OS_110_LEVEL|^|110|^|.body_boar|^|-1|^||^|LEVEL|^|n|^|1
151|^|sTIS_SET_AT_100_NAME_SKIN|^|-1|^||^|-1|^||^|AT_100_NAME_SKIN|^|s|^|Skin the creature
152|^|sTIS_SET_AT_100_NAME_SKIN_P|^|-1|^||^|-1|^||^|AT_100_NAME_SKIN_P|^|s|^|Skinning
153|^|sTIS_SET_AT_100_NAME_MEAT|^|-1|^||^|-1|^||^|AT_100_NAME_MEAT|^|s|^|Cut a piece of meat
154|^|sTIS_SET_AT_100_NAME_MEAT_P|^|-1|^||^|-1|^||^|AT_100_NAME_MEAT_P|^|s|^|Meat cutting
155|^|sTIS_SET_OS_110_AS_103_TYPE|^|110|^|.body_boar|^|103|^|.body_acquire_ingredA|^|TYPE|^|s|^|SKIN
156|^|nTIS_SET_OS_110_AS_103_RECIPE|^|110|^|.body_boar|^|103|^|.body_acquire_ingredA|^|RECIPE|^|n|^|130
157|^|sTIS_SET_OS_110_AS_104_TYPE|^|110|^|.body_boar|^|104|^|.body_acquire_ingredB|^|TYPE|^|s|^|MEAT
158|^|nTIS_SET_OS_110_AS_104_RECIPE|^|110|^|.body_boar|^|104|^|.body_acquire_ingredB|^|RECIPE|^|n|^|129
159|^|nTIS_SET_OS_110_AS_104_AMOUNT|^|110|^|.body_boar|^|104|^|.body_acquire_ingredB|^|AMOUNT|^|n|^|5
160|^|nTIS_SET_AS_104_QTY|^|-1|^||^|-1|^||^|AS_104_QTY|^|n|^|10000
161|^|sTIS_SET_OS_110_AS_126_DESC|^|110|^|.body_boar|^|126|^|.generic_examine|^|DESC|^|s|^|The corpse of a dead boar
162|^|nTIS_SET_OS_110_QTY|^|110|^|.body_boar|^|-1|^||^|QTY|^|n|^|5000
163|^|nTIS_SET_OS_111_LEVEL|^|111|^|.body_wolf|^|-1|^||^|LEVEL|^|n|^|1
164|^|sTIS_SET_OS_111_AS_103_TYPE|^|111|^|.body_wolf|^|103|^|.body_acquire_ingredA|^|TYPE|^|s|^|SKIN
165|^|nTIS_SET_OS_111_AS_103_RECIPE|^|111|^|.body_wolf|^|103|^|.body_acquire_ingredA|^|RECIPE|^|n|^|131
166|^|sTIS_SET_OS_111_AS_104_TYPE|^|111|^|.body_wolf|^|104|^|.body_acquire_ingredB|^|TYPE|^|s|^|MEAT
167|^|nTIS_SET_OS_111_AS_104_RECIPE|^|111|^|.body_wolf|^|104|^|.body_acquire_ingredB|^|RECIPE|^|n|^|129
168|^|nTIS_SET_OS_111_AS_104_AMOUNT|^|111|^|.body_wolf|^|104|^|.body_acquire_ingredB|^|AMOUNT|^|n|^|3
169|^|sTIS_SET_OS_111_AS_126_DESC|^|111|^|.body_wolf|^|126|^|.generic_examine|^|DESC|^|s|^|The corpse of a dead wolf
170|^|nTIS_SET_OS_111_QTY|^|111|^|.body_wolf|^|-1|^||^|QTY|^|n|^|3000
171|^||^||^||^||^||^||^||^|
172|^||^||^||^||^||^||^||^|
173|^||^||^||^||^||^||^||^|
174|^||^||^||^||^||^||^||^|
175|^||^||^||^||^||^||^||^|
176|^||^||^||^||^||^||^||^|
177|^||^||^||^||^||^||^||^|
178|^||^||^||^||^||^||^||^|
179|^||^||^||^||^||^||^||^|
180|^||^||^||^||^||^||^||^|
