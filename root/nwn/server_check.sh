#!/bin/sh
cd /home/virgil/nwn/

part="Ilandria 2"
procLine="$(ps -eo %p:%a |grep "$part" |head -n -1 |awk -F: '{print $1}')"
procId=$procLine

if [ -z "$procId" ]
then
    echo "Process died - restarting..."
    ./server_restart.sh
fi
