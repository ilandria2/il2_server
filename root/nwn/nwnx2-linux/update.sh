#!/bin/sh

set +e
rm -r archive *.so
set -e

wget -q -Ozip 'http://ci.swordcoast.net/job/nwnx2-linux/lastSuccessfulBuild/artifact/*zip*/archive.zip'

unzip -o -q zip

find archive -type f -name \*.so -exec mv -v {} . \;

rm -r zip archive

