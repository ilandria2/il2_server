#!/bin/sh
####### USAGE #########
#thisPath=$(realpath $0) ############## call this before cd
#cd /home/virgil/nwn/
#./logfile.sh $thisPath "Started" ##### sample log line

file=$1
line=$2
logFile=$(echo "$file" | sed "s/\..*/.log/")
logDate=$(date "+%Y-%m-%d %H:%M:%S")
echo $logDate": "$line >> $logFile
