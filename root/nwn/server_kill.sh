#!/bin/sh
part="[I]landria 2"
procLine="$(ps -eo %p:%a |grep "$part" |awk -F: '{print $1}')"
procId=$procLine

if [ -z "$procId" ]
then
    echo "Proess not running"
else
    echo "Killing process id: $procId"
    kill -9 $procId
fi

