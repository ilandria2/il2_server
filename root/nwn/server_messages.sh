#!/bin/sh
thisPath=$(realpath $0)
cd /home/virgil/nwn/

##################./logfile.sh $thisPath "Started"

db="il2_master"
sql="select msgRequestor, msgRequestorId, msgType, msgSubType, msgRawDataId, msgParam from client_messages where msgStatus = 'NEW' and msgEnvType = 0"

# we get all NEW messages
data="$(mysql --login-path=root -D "$db" -N -e "$sql")"

# no messages detected
if [ -z "$data" ]
then
    exit 0;

# we got some messages
else
    printf '%s\n' "$data" | while read -r mRequestor mRequestorId mType mSubType mRawDataId mParam
    do
        # register account
        if [ $mType = 0 ] && [ $mSubType = 0 ]; then
            # create server vault dir
            mkdir -p "./servervault/$mRequestor"
            mkdir -p "./servervault/$mRequestor/chars"

            # create playerACC db entry
            sql="insert into sys_players (PlayerID, PlayerACC) values (-1, '"$mRequestor"')"
            mysql --login-path=root -D nwn -N -e "$sql"

            # save update this message as COMPLETED
            sql="call save_message('"$mRequestor"',$mRequestorId,0,$mType,$mSubType,$mRawDataId,'"$mParam"','"COMPLETED"',@result)"
            mysql --login-path=root -D il2_master -N -e "$sql"

        # register character
        elif [ $mType = 1 ]; then
        {
            # 1. get last player id on player's account
            #sql="select max(PlayerID) from nwn.sys_players where PlayerACC = '"$mParam"'"
            #maxPid="$(mysql --login-path=root -D "$db" -N -e "$sql")"
            #maxPid=$(($maxPid + 1))

            # 2. generate and clear path for new character
            charFile="./servervault/$mRequestor/chars/il2_bic_$mRequestorId.bic"
            rm -f $charFile

            # 3. export blob object from database
            sql="select msgData from client_messages_rawdata where msgId = $mRawDataId"
            mysql --login-path=root -D "$db" -N --raw -e "$sql" > $charFile

            ## 4. delete existing blank account entry (first time account)
            #sql="delete from sys_players where PlayerID = -1 and PlayerACC = '"$mRequestor"'"
            #mysql --login-path=root -D nwn -N -e "$sql"

            ## 5. insert new character
            #sql="insert into sys_players (PlayerID, PlayerACC) values ($mRequestorId, '"$mRequestor"')"
            #mysql --login-path=root -D nwn -N -e "$sql"

            # 6. report success
            sql="call save_message('"$mRequestor"',$mRequestorId,0,$mType,$mSubType,$mRawDataId,'"$mParam"','"COMPLETED"',@result)"
            mysql --login-path=root -D il2_master -N -e "$sql"
        } || {
            # 1. report error
            sql="call save_message(0,$mType,$mSubType,'"$mParam"','"ERROR"',@result)"
            mysql --login-path=root -D il2_master -N -e "$sql"
        }

        # play character
        elif [ $mType = 6 ]; then
        {
            # copy last played characters back to the chars folder
            charFile="./servervault/$mRequestor/*.bic"
            mv -f $charFile ./servervault/$mRequestor/chars/

            # copy the requested character from chars to user folder
            charFile="./servervault/$mRequestor/chars/il2_bic_$mRequestorId.bic"
            cp $charFile ./servervault/$mRequestor/

            # report success
            sql="call save_message('"$mRequestor"',$mRequestorId,0,$mType,$mSubType,$mRawDataId,'"$mParam"','"COMPLETED"',@result)"
            mysql --login-path=root -D il2_master -N -e "$sql"
        }
        fi
    done
fi

###############./logfile.sh $thisPath "Finished"
