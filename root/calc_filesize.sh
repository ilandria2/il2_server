#!/bin/sh
####### USAGE #########
#./calc_filesize.sh file.doc  ##### calcs file size in bytes for file.doc

file=$1
stat -c%s "$file"
