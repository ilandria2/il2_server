#!/bin/sh
####### USAGE #########
#./update_file.sh 1 nwn/hak/file.doc  ##### downloads files.ilandria.com/content/<TEST|PUBLIC>/file.zip and decompresses it
### if the input file is .xml then the TEST|PUBLIC folder is not used
### if the TEST|PUBLIC folder is determined based on the first (isTest) parameter

####### INPUTS ###########

isTest=$1
filePath=$2

if [ -z $isTest ]; then
    echo "Usage:"
    echo "./update_file.sh <isTest> <filePath>"
    echo "./update_file.sh 1 nwn_test/hak/il2_tilesets_1.hak"
    exit
fi

####### Calculate url ######

nwnDir="nwn"

if [ $isTest = 1 ]; then
    nwnDir="nwn_test";
fi

fullPath=$(echo "$filePath" | sed "s/{NWN_DIR}/$nwnDir/")
file=$(basename "$fullPath")
tempFile=$(echo "$file" | sed "s/\.xml//")
#echo $tempFile
outExt=".xml"
# file name unchanged after .xml was removed
if [ "$tempFile" = "$file" ]; then
    outExt=".zip"
fi

outFile=$(echo "$fullPath" | sed "s/\..*/$outExt/")
./download_file.sh $isTest "$fullPath"

#echo "___ downloading $file"
tempFile=$(echo "$file" | sed "s/\.zip//")


#echo "____ $file vs $tempFile"
# file unchanged after .zip was removed
if [ "$tempFile" = "$file" ]; then
    ./decompress_file.sh "$outFile"
fi
