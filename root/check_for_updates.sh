#!/bin/sh
####### USAGE #########
##### returns a list of file paths that need file updates for the specified environment
#./check_for_updates.sh <isTest>

isTest=$1

## usage

if [ -z $isTest ]; then
    echo "Usage:"
    echo "./check_for_updates.sh <isTest>"
    echo "./check_for_updates.sh 1"
    exit
fi

## calc params
folder="PUBLIC"
fileXml="il2_content.xml"

if [ $isTest = 1 ]; then
    folder="TEST"
    fileXml="il2_content_test.xml"
fi

## echo operation
echo "Checking for updates on $folder environment..."

## download definition file
./download_file.sh $isTest "$fileXml"

## transform xml
fileItems="$(./transform_fileitems.sh "$fileXml")"

## parse list
row=0
printf '%s\n' "$fileItems" | \
awk 'NR >= 2 { print }' | \
while IFS=, read -r fPath fAbsolute fIsSensitive fSize fChecksum fMethod
do
    row=$(( $row + 1 ))
    check=$(./check_file.sh $fIsSensitive $isTest $fSize $fMethod $fChecksum "$fPath")

#echo "Check $fPath: $check"
#echo "./check_file.sh $fIsSensitive $isTest $fSize $fMethod $fChecksum \"$fPath\""
    # update needed
    if [ $check -gt 0 ]; then

        # filter out optional files
        tempFile=$(echo "$fPath" | sed "s/music//" | sed "s/\.exe//")

        if [ "$tempFile" = "$fPath" ]; then
            echo "File item #$row: Update needed for $fPath"

            # update file item
            ./update_file.sh $isTest "$fPath"

            # re-check after install
            check=$(./check_file.sh $fIsSensitive $isTest $fSize $fMethod $fChecksum "$fPath")

            if [ $check -gt 0 ]; then
                echo "Update still needed for $fPath after it was updated"
            fi

            echo ""
        fi
    fi
done

