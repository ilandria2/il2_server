#!/bin/sh
####### USAGE #########
#./download_file.sh 1 nwn/hak/file.doc  ##### downloads files.ilandria.com/content/<TEST|PUBLIC>/file.zip
### if the input file is .xml then the TEST|PUBLIC folder is not used
### if the TEST|PUBLIC folder is determined based on the first (isTest) parameter

####### INPUTS ###########

isTest=$1
filePath=$2

if [ -z $isTest ]; then
    echo "Usage:"
    echo "./download_file.sh <isTest> <filePath>"
    echo "./download_file.sh 1 nwn_test/hak/il2_tilesets_1.hak"
    exit
fi

####### Calculate url ######

file=$(basename "$filePath")
tempFile=$(echo "$file" | sed "s/\.xml//")
folder="" # root folder
outExt=".xml"
# file name unchanged after .xml was removed
if [ "$tempFile" = "$file" ]; then

    outExt=".zip"
    if [ $isTest = 0 ]; then
        folder="PUBLIC/"
    else
        folder="TEST/"
    fi
fi

outFile=$(echo "$filePath" | sed "s/\..*/$outExt/")
fileFolder=$folder$(basename "$outFile")
fileUrl="files.ilandria.com/content/"$fileFolder

#echo $filePath " - " $file " - " $fileUrl " - " $outFile
echo "Downloading \"$fileUrl\" to \"$outFile\"..."
rm -f "$filePath" && wget -q "$fileUrl" -O "$outFile"
