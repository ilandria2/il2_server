
#--ensure db exists
create database if not exists il2_master 
character set utf8 
collate utf8_unicode_ci;

#--switch to master db
use il2_master;

#-- support accented characters
SET NAMES cp1250;

#-- re-create tables
#--drop table if exists client_messages;			#-- client message requests
#--drop table if exists client_messages_rawdata;	#-- client message requests' raw data

/*log
select '> Tables deleted' as "Log";#--*/



create table if not exists
client_messages
(
	 msgId int not null auto_increment
	,msgRequestor varchar(32) not null
	,msgRequestorId int not null default -1
	,msgEnvType int not null
	,msgType int not null
	,msgSubType int not null
	,msgRawDataId int not null default -1
	,msgParam nvarchar(255) not null
	,msgStatus varchar(20) not null default 'NEW'
	,msgCreated timestamp not null default current_timestamp
	,msgUpdated timestamp not null default current_timestamp
	,primary key (msgId)
	,unique(msgRequestor, msgRequestorId, msgEnvType, msgType, msgSubType, msgRawDataId, msgParam)
) character set utf8 collate utf8_unicode_ci;



create table if not exists
client_messages_rawdata
(
	#-- once the ID is generated it is then inserted into the msgParam column of client_messages
	 msgId int auto_increment primary key
	,msgData blob not null
) character set utf8 collate utf8_unicode_ci;



/*log
select '> Tables created' as "Log";#--*/

#-- drop existing procedures

drop procedure if exists check_account;		#-- check if player account is registered
drop procedure if exists check_message;		#-- check status of a client message
drop procedure if exists save_message;		#-- save a new or update existing client message

#-- recreate procedures

delimiter $$
create procedure check_account(in envType int, in accName varchar(50), out result int)
begin
	#-- reset result var
	set @id = null;

	#-- prod env
	if envType = 0 then
		select playerID
		into @id
		from nwn.sys_players
		where playerACC = accName
		order by date desc
		limit 1;
	#-- test env
	else
		select playerID
		into @id
		from nwn_test.sys_players
		where playerACC = accName
		order by date desc
		limit 1;
	end if;

	#-- set result
	set result = not(isnull(@id));
end$$



create procedure check_message(in requestor varchar(32), in requestorId int, in eType int, in mType int, in mSubType int, in rawDataId int, in mParam nvarchar(255), in mStatus varchar(20), out result varchar(32))
begin
	#-- result result var
	set @status = null;

	select msgStatus
	into @status
	from client_messages
	where msgRequestor = requestor
	and msgRequestorId = requestorId
	and msgEnvType = eType
	and msgType = mType
	and msgSubType = mSubType
	and msgRawDataId = rawDataId
	and msgParam = mParam
	and case mStatus when '' then true else msgStatus = mStatus end;

	#-- set result
	set result = case mStatus when '' then @status else not(isnull(@status)) end;
end$$



create procedure save_message(in requestor varchar(32), in requestorId int, in eType int, in mType int, in mSubType int, in rawDataId int, in mParam nvarchar(255), in mStatus varchar(20), out result int)
begin
	#-- reset result
	set result = 0;

	#-- insert new data
	insert into client_messages (msgRequestor, msgRequestorId, msgEnvType, msgType, msgSubType, msgRawDataId, msgParam, msgStatus)
	values (requestor, requestorId, eType, mType, mSubType, rawDataId, mParam, mStatus)

	#-- update date for existing date
	on duplicate key 
	update msgUpdated = default, msgStatus = mStatus;

	#-- set result to success
	set result = 1;
end$$
delimiter ;

/*log
select '> Procedure created' as "Log";#--*/

#-- drop existing functions

drop function if exists split_str;



#-- create new functions

delimiter $$
CREATE FUNCTION split_str(
  x VARCHAR(255),
  delim VARCHAR(12),
  pos INT
)
RETURNS VARCHAR(255) DETERMINISTIC
BEGIN 
    RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
       LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
       delim, '');
END$$

DELIMITER ;


/*log
select '> Function created' as "Log";#--*/
