<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" encoding="ASCII"/>
<xsl:strip-space elements="*"/>
<xsl:template match="/">
	<xsl:value-of select="'Path,Absolute,IsSensitive,Size,Checksum,Method&#x0A;'"/>
    <xsl:for-each select="//*/FileItem">
		<xsl:value-of select="translate(Path, '\\', '/')"/>
		<xsl:value-of select="','"/>
		<xsl:value-of select="Path/@Absolute"/>
		<xsl:value-of select="','"/>
		<xsl:value-of select="IsSensitive"/>
		<xsl:value-of select="','"/>
		<xsl:value-of select="Size"/>
		<xsl:value-of select="','"/>
		<xsl:value-of select="Checksum"/>
		<xsl:value-of select="','"/>
		<xsl:value-of select="Checksum/@Method"/>
		<xsl:value-of select="'&#x0A;'"/>
    </xsl:for-each>
</xsl:template>
</xsl:stylesheet>
