#!/bin/sh
####### USAGE #########
#./decompress_file.sh nwn/hak/file.zip  ##### decompreses nwn/hak/file.zip on its current folder with overwrite

####### INPUTS ###########

filePath=$1

if [ -z "$filePath" ]; then
    echo "Usage:"
    echo "./decompress_file.sh <filePath>"
    echo "./decompress_file.sh nwn/hak/il2_tilesets_1.zip"
    exit
fi

####### Calculate url ######

file=$(basename "$filePath")
folder=$(echo "$filePath" | sed "s/\.*$file//")
tempFile=$(echo "$file" | sed "s/\.zip//")

# file unchanged after .zip was removed
if [ "$tempFile" = "$file" ]; then
    exit 0;
fi

#echo $filePath " - " $file " - " $folder " (" $tempFile" )"
echo "Decompressing \"$filePath\"..."
unzip -q -o "$filePath" -d "$folder" && rm -f "$filePath"
