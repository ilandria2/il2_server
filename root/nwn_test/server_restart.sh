#!/bin/sh
cd /home/virgil/nwn_test
./server_kill.sh
./server_loaddb.sh

cd ..
./check_for_updates.sh 1

cd /home/virgil/nwn_test
./server_start.sh
