#!/bin/sh
####### USAGE #########
#./calc_md5.sh file.doc  ##### calcs md5 for file.doc

file=$1
md5sum $file |awk '{ print $1 }'
