#!/bin/bash
############## USAGE ################
## ./checkfile.sh <is_sensitive> <is_test> <expected_size> <checksum_method>  <expected_checksum> <file_path_relative>
## ./checkfile.sh 0 1 123564 md5 fj23h42gf2u3h23423jhj {NWN_DIR}/folder/file.doc
isSensitive=$1 #### 1|0
isTest=$2      #### 1|0
expSize=$3     #### number
method=$4      #### md5
checksum=$5    #### fwijgierjgij34j534hu
relPath=$6     #### {NWN_DIR}/folder 1/file.xyz

### results:
## 0 = no update needed
## 1 = update needed
## 2 = missing

#echo $isSensitive $isTest $expSize $method $checksum $relPath

if [ -z $isSensitive ]; then
    echo "Usage:"
    echo "./checkfile.sh <is_sensitive> <is_test> <expected_size> <checksum_method>  <expected_checksum> <file_path_relative>"
    echo "./checkfile.sh 0 1 123564 md5 fj23h42gf2u3h23423jhj {NWN_DIR}/folder/file.doc"
    exit
fi

# get directory based on environment type parameter
nwnDir="nwn"
if [ $isTest = 1 ]; then
    nwnDir="nwn_test";
fi
##echo NWN DIR: $nwnDir


# change to home dir where nwn root dir is located
#cd ~

# convert relative path to full path using environment directory
fullPath=${relPath/{NWN_DIR\}/$nwnDir}
#echo "FP: "$fullPath

# file exists - compare
if [ -f "$fullPath" ]; then
    #echo 0

    # calculate file size
    fileSize=$(./calc_filesize.sh "$fullPath")
    #echo File size is $fileSize

    # file size is as expected
    if [ $fileSize = $expSize ]; then

        # file is senstiive - additionally calculate md5 checksum
        if [ $isSensitive = 1 ]; then
            fileMd5=$(./calc_md5.sh "$fullPath")
            #echo $fileMd5

            # file checksum is as expected - return 0
            if [ $fileMd5 = $checksum ]; then
                echo 0
                exit

            # file checksum is different - return 1
            else
                echo 1
                exit
            fi


        # file is not sensitive - return 0
        else
            echo 0
            exit
        fi

    # file size is different - return 1
    else
        echo 1
        exit
    fi


# file does not exist - return 2
else
    echo 2
    exit
fi


###############./logfile.sh $thisPath "Finished"
