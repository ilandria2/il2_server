
#### Test server
# m h  dom mon dow   command
0 5 * * 0 /home/virgil/nwn_test/vault_backup.sh
0 5 * * 0 /home/virgil/nwn_test/db_backup.sh
* * * * * /home/virgil/nwn_test/server_messages.sh
* * * * * sleep 15 && /home/virgil/nwn_test/server_messages.sh
* * * * * sleep 30 && /home/virgil/nwn_test/server_messages.sh
* * * * * sleep 45 && /home/virgil/nwn_test/server_messages.sh

#### Public server
# m h  dom mon dow   command
* * * * * /home/virgil/nwn/server_check.sh
0 5 * * 0 /home/virgil/nwn/vault_backup.sh
0 5 * * 0 /home/virgil/nwn/db_backup.sh
* * * * * /home/virgil/nwn/server_messages.sh
* * * * * sleep 15 && /home/virgil/nwn/server_messages.sh
* * * * * sleep 30 && /home/virgil/nwn/server_messages.sh
* * * * * sleep 45 && /home/virgil/nwn/server_messages.sh

