<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('SmfRestClient.php');

$secretKey = '1l4ndr142.F0rum@S3cr3tK3y';
$api = new SmfRestClient($secretKey);
$user = isset($_POST['user']) ? $_POST['user'] : null;
$pwd =  isset($_POST['password']) ? $_POST['password'] : null;
$email = '';
$info = $api->get_user($user);

if ($info->data != 'false')
{
//print_r($info->data);

  $user = $info->data->member_name;
  $email = $info->data->email_address;
}

if ($pwd == null)
{
	print "Logging out: $user<br />";
	$api->logout_user($user);
	$param = $user . ';';
	$success = '1';
}
else
{
	print "Logging with: $user and $pwd<br />";

	$loggedIn = $api->authenticate_user($user, $pwd);

	if ($loggedIn->data == 'false')
	{
	  $error = '';

	  if ($loggedIn->error != null)
		$error = $loggedIn->error;

	  $success = '0';
	  $param = $user . ';';
	}

	else
	{
	  $login = $api->login_user($user, 10);
	  $logonline = $api->log_onlineApi($user);
	  $success = $login->data == 'true' && $logonline->data == 'true' ? '1' : '0';
	  $error = $login->error != '' ? $login->error : $logonline->error;
	  $param = $error != '' ? '' : $email;
	}
}  
  print "RESULT=SMF~$user~-1~0~0~0~-1~" . $param . '~' . $success . '~' . $error;

?>