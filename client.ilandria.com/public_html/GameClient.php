<?php

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	$dbhost = 'server.ilandria.com';
	$dbport = '33060';
	$dbuser = 'root';
	$dbpwd = 'nwn.1l4ndr142';

	$msgRequestor = '';
	$msgRequestorId = '';
	$msgEnv = '';
	$msgType = '';
	$msgSubType = '';
	$msgRawDataId = '';
	$msgParameter = '';

	
	///////////////////////////////////////////
	// Parse parameters from binary data input
	///////////////////////////////////////////

	// binary data in a single parameter
	$isBinary = count($_POST) < 4;
	if ($isBinary)
	{
		// parse parameters
		$combined = file_get_contents('php://input');
		$count = ToInt32($combined, 0)['1'];
		$startData = ToInt32($combined, 4)['1'];
		$startRawData = ToInt32($combined, 8)['1'];
		$sizeTemp = 0;

		$rawData = '';
		$message = [];

		$rawData = substr($combined, $startRawData, strlen($combined) - $startRawData);

		for ($i = 0; $i < $count; $i++) {
			$sizeK = ToInt32($combined, 12 + 2 * $i * 4)['1'];
			$sizeV = ToInt32($combined, 12 + 2 * $i * 4 + 4)['1'];
			$key = substr($combined, $sizeTemp + $startData, $sizeK);
			$value = substr($combined, $sizeTemp + $startData + $sizeK, $sizeV);

			$message[$key] = $value;

			$sizeTemp += $sizeK + $sizeV;
		}
		//print_r($message);
		
		if (is_null($message) || count($message) < 7) die('ERROR: unable to parse binary data!<br />');
		
		$msgRequestor = $message['msgRequestor'];
		$msgRequestorId = $message['msgRequestorId'];
		$msgEnv = $message['msgEnv'];
		$msgType = $message['msgType'];
		$msgSubType = $message['msgSubType'];
		$msgRawDataId = $message['msgRawDataId'];
		$msgParameter = $message['msgParameter'];
	}

	
	//////////////////////////////////////////////
	// Parse parameters from get/post array input
	//////////////////////////////////////////////

	else
	{
		$msgRequestor = $_POST['msgRequestor'];
		$msgRequestorId = $_POST['msgRequestorId'];
		$msgEnv = $_POST['msgEnv'];
		$msgType = $_POST['msgType'];
		$msgSubType = $_POST['msgSubType'];
		$msgRawDataId = $_POST['msgRawDataId'];
		$msgParameter = $_POST['msgParameter'];
	}
	
	$database_game = $msgEnv == '0' || $msgEnv == 'False' ? 'nwn' : 'nwn_test';
	$database = 'il2_master';

	echo "envType before: $msgEnv ";
	$msgEnv = $msgEnv == 'True' ? '1' : '0';
	echo "db_game: $database_game, envType: $msgEnv ";
	
	//////////////////////////////////////////////
	// Validate parameters
	//////////////////////////////////////////////
	
	if (is_null($msgType)) 
		die('missing parameters!<br />');

	echo "MessageRequest: $msgRequestor,$msgRequestorId,$msgEnv,$msgType,$msgSubType,$msgRawDataId,$msgParameter<br />";

	
	/////////////////////////////
	// Connect to mysql database
	/////////////////////////////

	$mysqli = new mysqli($dbhost, $dbuser, $dbpwd, $database, $dbport);
	
	if ($mysqli->connect_error)
		die("Unable to connect to the server database!<br />" . $mysqli->connect_error . "<br />");

	/////////////////////////////////////////////////////////////////////////////
	// Check account
	// RESULTS: 0 = exists, 1 = creating, 2 = create submitted, or "ERROR: xxxx"
	/////////////////////////////////////////////////////////////////////////////
	if ($msgType == 0 && $msgSubType == 0) 
	{
		$return = Query($mysqli, "CALL check_account('$msgEnv','$msgRequestor',@result)", "@result");
		if (!IsError($return))
		{
			//	user already exists
			if ($return == 1)
				$return = 0;
			
			//	user doesn't exist - check message NEW in DB
			else
			{
				$return = Query($mysqli, "CALL check_message('$msgRequestor',$msgRequestorId,$msgEnv,$msgType,$msgSubType,$msgRawDataId,'$msgParameter','NEW',@result)", "@result");
				if (!IsError($return))
				{
					//	message not found - save message (new register request)
					if ($return == 0)
					{
						$return = Query($mysqli, "CALL save_message('$msgRequestor',$msgRequestorId,$msgEnv,$msgType,$msgSubType,$msgRawDataId,'$msgParameter','NEW',@result)", "@result");
						if (!IsError($return))
						{
							if ($return == 1)
								$return = 2;
						}// error save new
					}
				}// error check new
			}
		} // error check_account
	}	

	////////////////////////////////////////////////////////////////////////
	// Create character
	// RESULTS: 1 = existing request, 2 = new request or "ERROR: xxxx"
	////////////////////////////////////////////////////////////////////////
	else if ($msgType == '1') 
	{
		// check existing m_create_character message with NEW status
		$return = Query($mysqli, "CALL check_message('$msgRequestor',$msgRequestorId,$msgEnv,$msgType,$msgSubType,$msgRawDataId,'$msgParameter','NEW',@result)", "@result");
		
		//	existing message with NEW status not found - insert raw data
		if ($return == 0)
		{
			// upload raw data to database
			$rawDataEscaped = mysqli_real_escape_string($mysqli, $rawData);
			$return = Query($mysqli, "insert into client_messages_rawdata values (default, '$rawDataEscaped')");
			if (!IsError($return))
			{
				// get the latest ID from messages raw data
				$return = Query($mysqli, "select max(msgId) as lastId from client_messages_rawdata", "lastId");
				if (!IsError($return))
				{
					// create new m_create message with parameter = ID to link it
					if ($msgRawDataId == -1)
						$msgRawDataId = $return;
					$return = Query($mysqli, "CALL save_message('$msgRequestor',$msgRequestorId,$msgEnv,$msgType,$msgSubType,$return,'$msgParameter','NEW',@result)", "@result");
					if (!IsError($return))
						$return = 2;
				} // error get last id
			}// error insert raw data
		}
		
		// existing NEW message found
		else 
			$return = 1;
	}

	//////////////////////////////////////////////////////
	// Get new player id
	// RESULTS: 100000+ = new player id, or "ERROR: xxxx"
	//////////////////////////////////////////////////////
	else if ($msgType == '2') 
	{
		// get last player id
		$return = Query($mysqli, "select max(PlayerID) as lastId from $database_game.sys_players", "lastId");
		if (!IsError($return))
		{
			$BASE_ID = 100000;
			if ($return < $BASE_ID)
				$return = $BASE_ID;
			$newId = $return + 1;
			$return = Query($mysqli, "insert into $database_game.sys_players (PlayerID, PlayerACC, PlayerNAME) values ($newId, '$msgRequestor', '$msgParameter')");
			
			if (!IsError($return))
				$return = $newId;
			
		}// error get last player id
	}

	//////////////////////////////////////////////////////
	// Get existing player characters
	// RESULTS: base64 table or "ERROR: xxx"
	//////////////////////////////////////////////////////
	else if ($msgType == '3') 
	{
		// get existing player characters
		$return = Query($mysqli, "select PlayerID, PlayerNAME, RegisteredOn, CharHeadType, CharSkinColor, CharHairColor, CharGender, CharDescription, CharHitPoints from $database_game.sys_players where PlayerACC = '$msgRequestor' and PlayerID != -1", "#");
	}

	
	//////////////////////////////////////////////////////
	// Check message
	// RESULTS: response from check_message procedure
	//////////////////////////////////////////////////////
	else if ($msgType == '4') 
	{
		$params = explode('|', $msgParameter);
		$mRequestor = $params[0];
		$mRequestorId = $params[1];
		$mEnvType = $params[2] == 'True' ? 1 : 0;
		$mType = $params[3];
		$mSubType = $params[4];
		$mRawDataId = $params[5];
		$mParam = $params[6];
		$mStatus = $params[7];
		$query = "CALL check_message('$mRequestor',$mRequestorId,$mEnvType,$mType,$mSubType,$mRawDataId,'$mParam','$mStatus',@result)";
		
		// get check client message
		$return = Query($mysqli, $query, "@result");
	}

	
	//////////////////////////////////////////////////////
	// Save message
	// RESULTS: 0 = success or 1 = existing request found
	//////////////////////////////////////////////////////
	else if ($msgType == '5') 
	{
		$params = explode('|', $msgParameter);
		$mRequestor = $params[0];
		$mRequestorId = $params[1];
		$mEnvType = $params[2] == 'True' ? 1 : 0;
		$mType = $params[3];
		$mSubType = $params[4];
		$mRawDataId = $params[5];
		$mParam = $params[6];
		$mStatus = $params[7];
		
		if ($mStatus == '')
			$mStatus = 'NEW';
		
		$query = "CALL save_message('$mRequestor',$mRequestorId,$mEnvType,$mType,$mSubType,$mRawDataId,'$mParam','$mStatus',@result)";

		// get save client message
		$return = Query($mysqli, $query, "@result");
	}

	
	////////////////////////////////////////
	// Unsupported message
	////////////////////////////////////////
	
	else {		
		$mysqli->close();
		die('Unexpected message type!');
	}

	///////////////////////////////////////////////////
	// Print formatted result message for the caller
	///////////////////////////////////////////////////
	
	$error = '';
	if (IsError($return))
	{
		$error = $return;
		$return = 9;
	}
	
	echo "RESULT=GAME~$msgRequestor~$msgRequestorId~$msgEnv~$msgType~$msgSubType~$msgRawDataId~$msgParameter~$return~$error";

	$mysqli->close();

	
	////////////////////////////////////////
	// FUNCTIONS
	////////////////////////////////////////

	function ToInt32($data, $index)
	{
		return unpack('L', substr($data, $index, 4));
	}
	

	// Executes query on mysqli client and returns value of the @result variable
	function Query($mysqli, $query, $resultObject = null)
	{
		// @result variable query type - empty var first
		$resultVar = stripos($resultObject, "@") !== false;
		$resultTable = $resultObject == "#";

		if ($resultVar)
		{
			$queryVar = "SET $resultObject = ''";
			if (!$mysqli->query($queryVar))
				return "ERROR: Query [$queryVar] failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br />";
			
		}
		
		// execute query
		if (!($res = $mysqli->query($query)))
			return "ERROR: Query [$query] failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br />";
	
		// expected result from query
		if ($resultObject !== null)
		{
			// @result variable query type - select @result variable
			$resultVarName = $resultObject;
			if ($resultVar)
			{
				$resultVarName = 'result';
				$queryVar = "SELECT $resultObject as $resultVarName";
				if (!($res = $mysqli->query($queryVar))) 
					return "ERROR: Query [$queryVar] failed: (" . $mysqli->errno . ") " . $mysqli->error . "<br />";
			}
			
			// create simple html table structure
			if ($resultTable)
			{
				$resultVarName = '';
				$tableData = '';
				
				while ($row = $res->fetch_assoc()) 
				{
					$tableData .= "<tr>";
					
					foreach ($row as $column)
						$tableData .= "<td>$column</td>";

					$tableData .= "</tr>";
				}
				
				// data can contain characters such as param delimiters; so we encode it
				$return = base64_encode("<table>" . ($tableData) . "</table>");
			}
			else
			{
				$row = $res->fetch_assoc();
				$return = $row[$resultVarName];
			}
			
			return $return;
		}
		
		// return success when executing query without expecting result
		else 
			return true;
	}
	
	function IsError($value)
	{
		return stripos($value, "ERROR") !== false;
	}
?>